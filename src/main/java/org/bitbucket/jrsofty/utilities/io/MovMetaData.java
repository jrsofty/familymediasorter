package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.drew.imaging.ImageProcessingException;
import com.drew.imaging.quicktime.QuickTimeMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.mov.QuickTimeDirectory;

public class MovMetaData implements ImageDateExtractor {
    private static Logger LOG = LoggerFactory.getLogger(MovMetaData.class);
    private File imageFile;
    private Metadata metadata;

    private QuickTimeDirectory qtDirectory;

    public MovMetaData() {

    }

    @Override
    public void setFile(final File imageFile) throws MetaDataExtractionException {
        this.imageFile = imageFile;
        this.readMetaDataInformationFromImage();
    }

    @Override
    public Optional<Date> getOriginalDate() {
        Date result = null;
        MovMetaData.LOG.info("Calling getOriginalDate");
        if (this.qtDirectory.containsTag(QuickTimeDirectory.TAG_CREATION_TIME)) {
            MovMetaData.LOG.info("A value should be stored");
            result = this.qtDirectory.getDate(QuickTimeDirectory.TAG_CREATION_TIME);
            MovMetaData.LOG.info("Result added");
        }
        MovMetaData.LOG.info("Passing result into Optional");
        return Optional.ofNullable(result);

    }

    @Override
    public boolean hasOriginalDate() {

        return this.qtDirectory.containsTag(QuickTimeDirectory.TAG_CREATION_TIME);
    }

    @Override
    public Optional<Date> getDigitializedDate() {

        final Date result = null;
        // if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
        // result = this.ifd.getDate(ExifDirectoryBase.TAG_DATETIME_DIGITIZED);
        // } else if
        // (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
        // result = this.ifd0.getDate(ExifDirectoryBase.TAG_DATETIME_DIGITIZED);
        // }

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDigitalizedDate() {
        final boolean result = false;

        return result;
    }

    @Override
    public Optional<Date> getDefaultDate() {
        final Date result = null;

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDefaultDate() {
        final boolean result = false;

        return result;
    }

    private void readMetaDataInformationFromImage() throws MetaDataExtractionException {
        try {
            this.metadata = QuickTimeMetadataReader.readMetadata(this.imageFile);

            this.qtDirectory = this.metadata.getFirstDirectoryOfType(QuickTimeDirectory.class);

            if (this.qtDirectory == null) {
                throw new MetaDataExtractionException("The quick time reader cannot find the appropriate meta data directory");
            }

        } catch (ImageProcessingException | IOException e) {
            throw new MetaDataExtractionException("Failed to extract metadata from file " + this.imageFile.getAbsolutePath(), e);
        }
    }

}
