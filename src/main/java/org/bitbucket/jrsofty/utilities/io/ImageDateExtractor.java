package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.util.Date;
import java.util.Optional;

public interface ImageDateExtractor {

    void setFile(File file) throws MetaDataExtractionException;

    boolean hasOriginalDate();

    Optional<Date> getOriginalDate();

    boolean hasDigitalizedDate();

    Optional<Date> getDigitializedDate();

    boolean hasDefaultDate();

    Optional<Date> getDefaultDate();

}
