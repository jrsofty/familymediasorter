package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FolderWriter extends Thread {

    private static Logger LOG = LoggerFactory.getLogger(FolderWriter.class);

    private final LinkedBlockingQueue<String> queue;
    private final Properties properties;
    public volatile boolean running = false;
    private final DateFormat yearFormat = new SimpleDateFormat("yyyy");
    private final DateFormat monthFormat = new SimpleDateFormat("MM");
    private final DateFormat dayFormat = new SimpleDateFormat("dd");
    private int filesWritten = 0;
    private final String[] supportedImg;
    private final String[] supportedVid;
    private final HashSet<String> foldersToDelete = new HashSet<>();

    /**
     * The constructor for the FolderWriter class. Which begins copying files
     * from the source to the target directory.
     *
     * @param properties
     *            that define the operation of the file writer
     * @param queue
     *            the queue of files loaded by the FileReader thread.
     */
    public FolderWriter(final Properties properties, final LinkedBlockingQueue<String> queue) {
        super("FolderWriter");
        this.properties = properties;
        this.queue = queue;
        FolderWriter.LOG.info("Writer initialized");
        this.supportedImg = properties.getProperty("supp.img").split(",");
        this.supportedVid = properties.getProperty("supp.vid").split(",");
    }

    private void calculatePercentageAndOutput() {
        if ((this.filesWritten % 100) == 0) {
            System.out.println(this.filesWritten + " files written");
        }
    }

    private String checkAndReturnNameDuplication(final String targetPath, final String filename) {
        File testFile = new File(targetPath + filename);
        if (!testFile.exists()) {
            return targetPath + filename;
        }

        int bump = 0;
        String newFileName;
        do {
            newFileName = String.valueOf(bump) + "_" + filename;
            testFile = new File(targetPath + newFileName);
            bump++;
        }
        while (testFile.exists());
        FolderWriter.LOG.info("Changed file name to: " + newFileName);
        return targetPath + newFileName;
    }

    private void createDirectories(final String path) {
        final File directory = new File(path);
        if (!directory.exists()) {
            FolderWriter.LOG.info("Creating path: " + path);
            directory.mkdirs();
        }
    }

    private String createNewTargetPath(final Date date, final String subPath) {
        final String rootPath = this.getTargetDirectoryWithSeparator();
        final StringBuffer pathBuilder = new StringBuffer(rootPath);

        pathBuilder.append(subPath + "/");

        pathBuilder.append(this.yearFormat.format(date) + "/");
        pathBuilder.append(this.monthFormat.format(date) + "/");
        pathBuilder.append(this.dayFormat.format(date) + "/");

        return pathBuilder.toString();
    }

    private String createNewTargetPathImage(final Date date) {
        String path = null;
        if (date == null) {
            path = this.createErrorPath();
        } else {
            path = this.createNewTargetPath(date, "pictures");
        }

        return path;
    }

    private String createNewTargetPathVideo(final Date date) {
        String path = null;
        if (date == null) {
            path = this.createErrorPath();
        } else {
            path = this.createNewTargetPath(date, "video");
        }
        return path;
    }

    private String createErrorPath() {
        return this.getTargetDirectoryWithSeparator() + "ERRORS/";
    }

    private void fileCopy(final Path sourcePath, final Path targetPath) throws IOException {
        Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING, LinkOption.NOFOLLOW_LINKS);
    }

    private void fileMove(final Path sourcePath, final Path targetPath) throws IOException {
        Files.move(sourcePath, targetPath, StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING);
    }

    private String getExtension(final String fileName) {
        final int lastDot = fileName.lastIndexOf('.') + 1;
        return fileName.substring(lastDot).toLowerCase();

    }

    private String getTargetDirectoryWithSeparator() {
        final String rootPath = this.properties.getProperty("target.dir");
        if (rootPath.endsWith("/") || rootPath.endsWith("\\")) {
            return rootPath;
        }

        return rootPath + "/";
    }

    private void processFile(final File originalFile, final String path) throws IOException {

        final String fullNewPath = this.checkAndReturnNameDuplication(path, originalFile.getName());
        final Path targetFilePath = Paths.get(fullNewPath);
        final Path sourceFilePath = originalFile.toPath();

        if (this.properties.getProperty("option.copy").equals("true")) {
            FolderWriter.LOG.info("Copy file to: " + targetFilePath);
            this.fileCopy(sourceFilePath, targetFilePath);
        } else {
            FolderWriter.LOG.info("Move file to: " + targetFilePath);
            this.fileMove(sourceFilePath, targetFilePath);
            final String parentPath = originalFile.getParent();
            this.foldersToDelete.add(parentPath);
        }

        if (this.properties.getProperty("option.outputFile").equals("true")) {
            TrackingWriter.getInstance().addTargetFile(originalFile.getAbsolutePath(), fullNewPath);
        }
    }

    private void processFile(final String path) throws IOException, MetaDataExtractionException {
        if (path.equals("<<END>>")) {
            FolderWriter.LOG.debug("End of Queue identified");
            return;
        }
        final File file = new File(path);
        final String extension = this.getExtension(file.getName());
        final ImageDateExtractor dateExtractor = this.buildExtractorForExtension(extension);
        dateExtractor.setFile(file);
        // final JpegMetaData dateExtractor = new JpegMetaData(file);
        Date fileDate = null;
        try {
            if (dateExtractor.hasOriginalDate()) {
                FolderWriter.LOG.debug("Attempting to get the original date");
                fileDate = dateExtractor.getOriginalDate().get();
            } else if (dateExtractor.hasDigitalizedDate()) {
                fileDate = dateExtractor.getDigitializedDate().get();
            } else if (dateExtractor.hasDefaultDate()) {
                fileDate = dateExtractor.getDefaultDate().get();
            } else {
                fileDate = this.getFileSystemDate(file);
            }
        } catch (final Exception e) {
            FolderWriter.LOG.error("Failed to identify a valid date for image: " + file.getAbsolutePath(), e);
            fileDate = this.getFileSystemDate(file);
            // throw new MetaDataExtractionException("Could not get valid date",
            // e);
        }

        String newTargetPath = "";

        if (Arrays.asList(this.supportedImg).contains(extension)) {
            newTargetPath = this.createNewTargetPathImage(fileDate);
        } else if (Arrays.asList(this.supportedVid).contains(extension)) {
            newTargetPath = this.createNewTargetPathVideo(fileDate);
        } else {
            FolderWriter.LOG.warn(file.getName() + " has an unsupported extension.");
            newTargetPath = this.createErrorPath();
        }

        this.createDirectories(newTargetPath);
        this.processFile(file, newTargetPath);

    }

    private ImageDateExtractor buildExtractorForExtension(final String extension) {
        ImageDateExtractor extractor = null;
        if (extension.equals("mov")) {
            extractor = new MovMetaData();
        } else if (extension.equals("mp4")) {
            extractor = new Mp4MetaData();
        } else if (extension.equals("nef")) {
            extractor = new TiffMetaData();
        } else if (extension.equals("arw")) {
            extractor = new TiffMetaData();
        } else {
            extractor = new JpegMetaData();
        }

        return extractor;
    }

    @Override
    public void run() {
        FolderWriter.LOG.info("Begin file processing");
        this.running = true;
        String path = "<<BEGIN>>";
        try {
            while (!path.equals("<<END>>")) {
                path = this.queue.take();
                this.processFile(path);
                this.filesWritten++;
                this.calculatePercentageAndOutput();

            }

        } catch (final InterruptedException | IOException | MetaDataExtractionException e) {
            System.out.println("An error occured while writing a file. Please view logs for details");
            FolderWriter.LOG.error("Error while processing " + path, e);
        }

        for (final String folder : this.foldersToDelete) {
            final File file = new File(folder);
            file.delete();
        }

        this.running = false;
        FolderWriter.LOG.debug("Writer thread ended");
        System.out.println("Total files written: " + this.filesWritten);
    }

    private Date getFileSystemDate(final File imageFile) throws IOException {
        final Path filePath = Paths.get(imageFile.getAbsolutePath());
        final BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);

        final Date fileSystemDate = new Date(attributes.creationTime().toMillis());

        return fileSystemDate;
    }

}
