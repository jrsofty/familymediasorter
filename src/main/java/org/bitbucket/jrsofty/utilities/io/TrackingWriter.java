package org.bitbucket.jrsofty.utilities.io;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackingWriter {
    private static Logger LOG = LoggerFactory.getLogger(TrackingWriter.class);
    private static TrackingWriter instance = new TrackingWriter();

    public static TrackingWriter getInstance() {
        return TrackingWriter.instance;
    }

    private final ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();

    private String outputFileName = null;

    private TrackingWriter() {

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmmss");

        this.outputFileName = "Organizer_" + sdf.format(new Date()) + ".txt";
        TrackingWriter.LOG.debug("Initialized optional output file: " + this.outputFileName);
    }

    public synchronized void addTargetFile(final String sourceFile, final String targetFile) {
        TrackingWriter.LOG.debug("Adding " + sourceFile + " >> " + targetFile);
        this.map.put(sourceFile, targetFile);
    }

    private synchronized String buildOutput() {
        final StringBuffer buffer = new StringBuffer();

        final Iterator<Entry<String, String>> itr = this.map.entrySet().iterator();
        while (itr.hasNext()) {
            final Entry<String, String> entry = itr.next();
            buffer.append(entry.getKey() + "\t" + entry.getValue() + System.getProperty("line.separator"));
        }

        return buffer.toString();
    }

    /**
     * Writes the output file when the files copy process is complete.
     *
     * @throws UnsupportedEncodingException
     *             when the output text uses a unsupported encoding.
     * @throws IOException
     *             when an error occurs during the write process.
     */
    public synchronized void writeOutputFile() throws UnsupportedEncodingException, IOException {
        TrackingWriter.LOG.debug("Begin output file writing");
        final String outputText = this.buildOutput();
        Files.write(Paths.get(this.outputFileName), outputText.getBytes("utf-8"), StandardOpenOption.CREATE);
        TrackingWriter.LOG.debug("Output file written");
        System.out.println("Output file is named: " + this.outputFileName);
    }

}
