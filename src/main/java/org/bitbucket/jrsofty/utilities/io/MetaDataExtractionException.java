package org.bitbucket.jrsofty.utilities.io;

import java.io.File;

public class MetaDataExtractionException extends Exception {

    private static final long serialVersionUID = 3758129501561960704L;

    public MetaDataExtractionException(final File file) {
        super("Could not extract the requested information from " + file.getAbsolutePath());
    }

    public MetaDataExtractionException(final File file, final Throwable t) {
        super("Could not extract the request information from " + file.getAbsolutePath(), t);
    }

    public MetaDataExtractionException(final String message) {
        super(message);
    }

    public MetaDataExtractionException(final String message, final Throwable t) {
        super(message, t);
    }

}
