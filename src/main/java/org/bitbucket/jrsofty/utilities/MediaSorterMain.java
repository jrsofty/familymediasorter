package org.bitbucket.jrsofty.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.bitbucket.jrsofty.utilities.io.FolderReader;
import org.bitbucket.jrsofty.utilities.io.FolderWriter;
import org.bitbucket.jrsofty.utilities.io.ParametersNotValidException;
import org.bitbucket.jrsofty.utilities.io.TrackingWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MediaSorterMain {

    private static Logger LOG = LoggerFactory.getLogger(MediaSorterMain.class);

    /**
     * Main application method for starting the application. This static method
     * creates an instance and starts the processing. Valid arguments and
     * switches [OPTIONS] &lt;source_dir&gt; &lt;target_dir&gt;
     * <ul>
     * <li>M = Move, copies the files from source to target directories and
     * deletes the files after move.
     * <li>C = Copy, copies the files from source to target directories but
     * leaves source files in place.
     * <li>More to come ...
     * </ul>
     *
     * @param args
     *            as String array
     */
    public static void main(final String[] args) {
        MediaSorterMain.LOG.info("Begin application");
        System.out.println("Begin operation");
        final MediaSorterMain application = new MediaSorterMain();
        System.out.println("Processing ... ");
        application.process(args);
        System.out.println("Operation complete");
        MediaSorterMain.LOG.info("Application ended");
        System.exit(0);
    }

    private final Properties properties = new Properties();

    private MediaSorterMain() {

    }

    private void parseArgs(final String[] args) throws ParametersNotValidException {
        if (args.length < 3) {
            System.out.println("Not enough parameters");
            throw new ParametersNotValidException("Not enough parameters");
        }

        if (!args[0].startsWith("-")) {
            System.out.println("Options not first parameter");
            throw new ParametersNotValidException("Options not first parameter");
        }
        this.parseOptions(args[0].substring(1));
        this.properties.setProperty("source.dir", args[1]);
        this.properties.setProperty("target.dir", args[2]);

    }

    private void parseOptions(final String options) {

        for (int i = 0; i < options.length(); i++) {
            final char c = options.charAt(i);
            if (c == 'M') {
                this.properties.setProperty("option.move", "true");
                this.properties.setProperty("option.copy", "false");
            }
            if (c == 'C') {
                this.properties.setProperty("option.move", "false");
                this.properties.setProperty("option.copy", "true");
            }
            if (c == 'O') {
                this.properties.setProperty("option.outputFile", "true");
            }
            if (c == 'V') {
                this.properties.setProperty("option.video", "true");
            }
        }

        if (!this.properties.containsKey("option.outputFile")) {
            this.properties.setProperty("option.outputFile", "false");

        }
        if (!this.properties.containsKey("option.video")) {
            this.properties.setProperty("option.video", "false");
        }

    }

    /**
     * Method which starts the file read/copy handling as well as prepares the
     * parameters as sent via the command line.
     *
     * @param args
     *            a String array from the command line parameters.
     */
    public void process(final String[] args) {

        final FolderReader reader;
        final FolderWriter writer;
        final LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>();
        final InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream("supportedTypes.properties");

        try {
            this.properties.load(stream);
            this.parseArgs(args);
            reader = new FolderReader(this.properties, queue);
            reader.start();

            writer = new FolderWriter(this.properties, queue);
            writer.start();
            do {
                Thread.sleep(1000);
            }
            while (writer.running == true);

            if (this.properties.getProperty("option.outputFile").equals("true")) {
                try {
                    TrackingWriter.getInstance().writeOutputFile();
                } catch (final IOException e) {
                    MediaSorterMain.LOG.error("Failed to write output file", e);
                }
            }

        } catch (final ParametersNotValidException e) {
            System.out.println("The parameters are invalid: " + e.getMessage());
            MediaSorterMain.LOG.error("Invalid parameters", e);
        } catch (final InterruptedException e) {
            System.out.println("Stopped");
            MediaSorterMain.LOG.error("Stopped", e);
        } catch (final IOException e) {
            MediaSorterMain.LOG.error("Could not find default properties file", e);
            System.out.println("Internal Error x1000: Contact vendor");
        }

    }

}
