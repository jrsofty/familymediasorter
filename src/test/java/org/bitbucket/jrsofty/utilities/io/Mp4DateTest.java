package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Mp4DateTest {

    @Test
    void requestCreationTimeFromMp4() throws MetaDataExtractionException {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/example1.mp4").getFile());
        final ImageDateExtractor extractor = new Mp4MetaData();
        extractor.setFile(file);
        final Optional<Date> creationDate = extractor.getOriginalDate();
        final Date result = creationDate.get();
        System.out.println(sdf.format(result));
        Assertions.assertNotNull(result);
    }

}
