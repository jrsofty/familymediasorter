package org.bitbucket.jrsofty.utilities;

import java.io.Console;

public class CommandLineOutput {

    private final Console console;

    public CommandLineOutput() {
        this.console = System.console();
    }

    public void println(final String message) {
        if (this.console != null) {
            this.console.writer().write(message);
            this.console.writer().write(System.lineSeparator());
            this.console.flush();
        } else {
            System.out.println(message);
        }
    }

}
