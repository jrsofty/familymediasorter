package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class JpegMetaData implements ImageDateExtractor {

    private File imageFile;
    private Metadata metadata;
    private ExifSubIFDDirectory ifd;
    private ExifIFD0Directory ifd0;

    public JpegMetaData() {

    }

    @Override
    public void setFile(final File imageFile) throws MetaDataExtractionException {
        this.imageFile = imageFile;
        this.readMetaDataInformationFromImage();
    }

    @Override
    public Optional<Date> getOriginalDate() {
        Date result = null;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME_ORIGINAL)) {
            result = this.ifd.getDate(ExifDirectoryBase.TAG_DATETIME_ORIGINAL);
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME_ORIGINAL)) {
            result = this.ifd0.getDate(ExifDirectoryBase.TAG_DATETIME_ORIGINAL);
        }
        return Optional.ofNullable(result);

    }

    @Override
    public boolean hasOriginalDate() {
        boolean result = false;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME_ORIGINAL)) {
            result = true;
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME_ORIGINAL)) {
            result = true;
        }
        return result;
    }

    @Override
    public Optional<Date> getDigitializedDate() {

        Date result = null;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
            result = this.ifd.getDate(ExifDirectoryBase.TAG_DATETIME_DIGITIZED);
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
            result = this.ifd0.getDate(ExifDirectoryBase.TAG_DATETIME_DIGITIZED);
        }

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDigitalizedDate() {
        boolean result = false;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
            result = true;
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME_DIGITIZED)) {
            result = true;
        }
        return result;
    }

    @Override
    public Optional<Date> getDefaultDate() {
        Date result = null;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME)) {
            result = this.ifd.getDate(ExifDirectoryBase.TAG_DATETIME);
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME)) {
            result = this.ifd0.getDate(ExifDirectoryBase.TAG_DATETIME);
        }

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDefaultDate() {
        boolean result = false;
        if (this.ifd.containsTag(ExifDirectoryBase.TAG_DATETIME)) {
            result = true;
        } else if (this.ifd0.containsTag(ExifDirectoryBase.TAG_DATETIME)) {
            result = true;
        }
        return result;
    }

    private void readMetaDataInformationFromImage() throws MetaDataExtractionException {
        try {
            this.metadata = JpegMetadataReader.readMetadata(this.imageFile);
            this.ifd0 = this.metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            this.ifd = this.metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);

        } catch (JpegProcessingException | IOException e) {
            throw new MetaDataExtractionException("Failed to extract metadata from file " + this.imageFile.getAbsolutePath(), e);
        }
    }

}
