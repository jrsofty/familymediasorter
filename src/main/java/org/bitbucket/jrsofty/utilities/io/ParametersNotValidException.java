package org.bitbucket.jrsofty.utilities.io;

public class ParametersNotValidException extends Exception {

    private static final long serialVersionUID = 815584076257098877L;

    public ParametersNotValidException(final String message) {
        super(message);

    }

}
