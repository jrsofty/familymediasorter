package org.bitbucket.jrsofty.utilities.io;

import java.io.File;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ImageDateExtractorTest {

    @Test
    public void testValidDefaultDateAvailable1() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/wrong-date.jpg").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDefaultDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testFailedOriginalDateAvailable() throws MetaDataExtractionException {
        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/wrong-date.jpg").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasOriginalDate();
        Assertions.assertFalse(result);
    }

    @Test
    public void testFailedDigitizedDateAvailable() throws MetaDataExtractionException {
        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/wrong-date.jpg").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDigitalizedDate();
        Assertions.assertFalse(result);
    }

    @Test
    public void testValidOriginalDateAvailable() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/valid.JPG").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasOriginalDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testValidDefaultDateAvailable2() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/valid.JPG").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDefaultDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testValidDigitizedDateAvailable() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/valid.JPG").getFile());
        final JpegMetaData impl = new JpegMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDigitalizedDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testNikonRawDefaultDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/nikon-raw-test.NEF").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDefaultDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testSonyRawDefaultDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/sony-raw-test.ARW").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDefaultDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testNikonRawDigitizedDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/nikon-raw-test.NEF").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDigitalizedDate();
        Assertions.assertFalse(result);
    }

    @Test
    public void testSonyRawDigitizedDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/sony-raw-test.ARW").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasDigitalizedDate();
        Assertions.assertFalse(result);
    }

    @Test
    public void testNikonRawOriginalDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/nikon-raw-test.NEF").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasOriginalDate();
        Assertions.assertTrue(result);
    }

    @Test
    public void testSonyRawOriginalDate() throws MetaDataExtractionException {

        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/sony-raw-test.ARW").getFile());
        final TiffMetaData impl = new TiffMetaData();
        impl.setFile(file);
        final boolean result = impl.hasOriginalDate();
        Assertions.assertFalse(result);
    }

}
