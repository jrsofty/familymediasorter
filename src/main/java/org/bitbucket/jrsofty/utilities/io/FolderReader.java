package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FolderReader extends Thread {
    private static Logger LOG = LoggerFactory.getLogger(FolderReader.class);
    private final String sourcePath;
    private final LinkedBlockingQueue<String> queue;
    public volatile boolean running = false;
    private final List<String> crapToDelete = new ArrayList<>();
    private boolean videos = false;
    private int filesFound = 0;
    private int crapDeleted = 0;
    private final String[] supportedImg;
    private final String[] supportedVid;

    /**
     * Constructor of thread, which prepares the folder reasing service.
     * 
     * @param properties
     *            as Properties file which sets the configuration for the reader
     * @param queue
     *            as LinkedBlockingQueue providing where the file paths for
     *            handling are stored.
     */
    public FolderReader(final Properties properties, final LinkedBlockingQueue<String> queue) {
        super("FolderReader");
        this.sourcePath = properties.getProperty("source.dir");
        this.queue = queue;
        this.supportedImg = properties.getProperty("supp.img").split(",");
        this.supportedVid = properties.getProperty("supp.vid").split(",");

        if (properties.getProperty("option.video").equals("true")) {
            this.videos = true;
        }
        FolderReader.LOG.info("Folder reader initialized");
    }

    private void deleteCrap() {
        for (final String path : this.crapToDelete) {
            try {
                final File f = new File(path);
                f.delete();
                this.crapDeleted++;
            } catch (final Exception e) {
                FolderReader.LOG.error("Cannot delete path: " + path, e);
            }
        }
    }

    private String getFileExtension(final File entry) {
        final int lastDot = entry.getName().lastIndexOf('.') + 1;
        final String extension = entry.getName().substring(lastDot).toLowerCase();
        return extension;
    }

    private void processEntry(final File entry) throws InterruptedException {
        final String filePath = entry.getAbsolutePath();
        final String extension = this.getFileExtension(entry).toLowerCase().trim();
        if (entry.isDirectory()) {
            this.readFilePathsFromDirectory(entry.getAbsolutePath());
        } else if (Arrays.asList(this.supportedImg).contains(extension)) {
            this.queue.put(entry.getAbsolutePath());
            this.filesFound++;
        } else if (this.videos && Arrays.asList(this.supportedVid).contains(extension)) {
            this.queue.put(entry.getAbsolutePath());
            this.filesFound++;
        } else if (filePath.toLowerCase().endsWith(".db") || filePath.toLowerCase().endsWith(".ini")) {
            this.crapToDelete.add(entry.getAbsolutePath());
            FolderReader.LOG.warn("Windows Crap Found: " + filePath);
        } else {
            FolderReader.LOG.error("Unknown or unsupported file type: " + filePath);
        }
    }

    private void readFilePathsFromDirectory(final String path) {

        final File directory = new File(path);
        final File[] subEntries = directory.listFiles();
        FolderReader.LOG.info("Loading: " + subEntries.length + " from " + path);
        System.out.println("Loading: " + subEntries.length + " from " + path);
        for (final File entry : subEntries) {
            try {
                this.processEntry(entry);
            } catch (final InterruptedException e) {
                FolderReader.LOG.error("Interrupted during process entry", e);
            }
        }
        FolderReader.LOG.info("Files Found: " + this.filesFound);
    }

    @Override
    public void run() {
        System.out.println("Read process begins ... ");
        FolderReader.LOG.info("Reader thread started for path " + this.sourcePath);
        this.running = true;
        this.readFilePathsFromDirectory(this.sourcePath);
        this.deleteCrap();
        FolderReader.LOG.info("Thumbs.db or .ini files delete: " + this.crapDeleted);
        try {
            this.queue.put("<<END>>");
        } catch (final InterruptedException e) {
            FolderReader.LOG.error("Interrupted while ending queue", e);
        }
        FolderReader.LOG.info(this.queue.size() + " entries queued!");

        this.running = false;

        FolderReader.LOG.debug("Reader thread ended");
        System.out.println("Read process ends.");
    }

}
