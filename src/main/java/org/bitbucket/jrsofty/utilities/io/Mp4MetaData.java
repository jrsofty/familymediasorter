package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.drew.imaging.ImageProcessingException;
import com.drew.imaging.mp4.Mp4MetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.mp4.Mp4Directory;

public class Mp4MetaData implements ImageDateExtractor {

    private static Logger LOG = LoggerFactory.getLogger(Mp4MetaData.class);

    private File imageFile;
    private Metadata metadata;
    private Mp4Directory mp4Directory;

    public Mp4MetaData() {

    }

    @Override
    public void setFile(final File imageFile) throws MetaDataExtractionException {
        this.imageFile = imageFile;
        this.readMetaDataInformationFromImage();
    }

    @Override
    public Optional<Date> getOriginalDate() {
        Mp4MetaData.LOG.debug("Getting mp4 date");
        Date result = null;
        if (this.mp4Directory.hasTagName(Mp4Directory.TAG_CREATION_TIME)) {
            Mp4MetaData.LOG.debug("Date should be available");
            result = this.mp4Directory.getDate(Mp4Directory.TAG_CREATION_TIME);
            Mp4MetaData.LOG.debug(null == result ? "yes" : "no");
        }
        Mp4MetaData.LOG.debug("Optional being set");
        return Optional.ofNullable(result);

    }

    @Override
    public boolean hasOriginalDate() {
        return this.mp4Directory.hasTagName(Mp4Directory.TAG_CREATION_TIME);
    }

    @Override
    public Optional<Date> getDigitializedDate() {

        final Date result = null;

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDigitalizedDate() {
        final boolean result = false;

        return result;
    }

    @Override
    public Optional<Date> getDefaultDate() {
        final Date result = null;

        return Optional.ofNullable(result);
    }

    @Override
    public boolean hasDefaultDate() {
        final boolean result = false;

        return result;
    }

    private void readMetaDataInformationFromImage() throws MetaDataExtractionException {
        try {
            this.metadata = Mp4MetadataReader.readMetadata(this.imageFile);
            this.mp4Directory = this.metadata.getFirstDirectoryOfType(Mp4Directory.class);
        } catch (ImageProcessingException | IOException e) {
            throw new MetaDataExtractionException("Failed to extract metadata from file " + this.imageFile.getAbsolutePath(), e);
        }
    }

}
