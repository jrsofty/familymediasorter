package org.bitbucket.jrsofty.utilities.io;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MovDateTest {

    @Test
    void testGetCreationDateFromMOV() throws MetaDataExtractionException {
        final File file = new File(this.getClass().getClassLoader().getResource("date-test-image/DSC_5850.MOV").getFile());

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ImageDateExtractor extractor = new MovMetaData();
        extractor.setFile(file);
        final Optional<Date> creationDate = extractor.getOriginalDate();
        final Date result = creationDate.get();
        System.out.println(sdf.format(result));
        Assertions.assertNotNull(result);
    }

}
